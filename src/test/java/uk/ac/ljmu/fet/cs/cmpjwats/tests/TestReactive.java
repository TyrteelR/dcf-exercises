package uk.ac.ljmu.fet.cs.cmpjwats.tests;

import static org.junit.Assert.*;

import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;

import hu.mta.sztaki.lpds.cloud.simulator.DeferredEvent;
import hu.mta.sztaki.lpds.cloud.simulator.Timed;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.unimiskolc.iit.distsys.ExercisesBase;
import hu.unimiskolc.iit.distsys.interfaces.CloudProvider;
import uk.ac.ljmu.fet.cs.cmpjwats.MyReactiveProvider;

public class TestReactive {

	@Test
	public void testForDropOuts() throws Exception {
		final IaaSService myIaaS = ExercisesBase.getComplexInfrastructure(30);
		CloudProvider prov = new MyReactiveProvider();
		prov.setIaaSService(myIaaS);
		new DeferredEvent(2 * 60 * 60 * 1000) {
			@Override
			protected void eventAction() {
				int whichMachine = RandomUtils.nextInt(0, myIaaS.machines.size());
				try {
					myIaaS.deregisterHost(myIaaS.machines.get(whichMachine));
				} catch (Exception e) {

				}
			}
		};
		Timed.simulateUntilLastEvent();
		assertEquals("The machine count decreased!", 30, myIaaS.machines.size());
	}

	@Test
	public void testForUsage() throws Exception {
			// To be done!
	}
}
