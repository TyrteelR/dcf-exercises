package uk.ac.ljmu.fet.cs.cmpjwats.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.unimiskolc.iit.distsys.ExercisesBase;
import hu.unimiskolc.iit.distsys.interfaces.CloudProvider;
import uk.ac.ljmu.fet.cs.cmpjwats.MyFirstProvider;

public class TestForProviderReduction {

	@Test
	public void test() throws Exception {
		IaaSService iaas = ExercisesBase.getComplexInfrastructure(30);
		CloudProvider prov = new MyFirstProvider();
		prov.setIaaSService(iaas);
		assertEquals("Trouble with Harry", 15, iaas.machines.size());
	}

}
