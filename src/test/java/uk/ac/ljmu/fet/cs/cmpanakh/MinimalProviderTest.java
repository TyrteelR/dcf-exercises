package uk.ac.ljmu.fet.cs.cmpanakh;

import static org.junit.Assert.*;

import org.junit.Test;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ConstantConstraints;
import hu.unimiskolc.iit.distsys.BuiltInCloudProvider;
import hu.unimiskolc.iit.distsys.interfaces.CloudProvider;
import uk.ac.ljmu.fet.cs.cmpjwats.MinimalProvider;

/**
 * This is a test case for MinimalProvider (SUT).
 * 
 * @author cmpgkecs and changes cmpjwats
 *
 */
public class MinimalProviderTest {
	/**
	 * Instantiates our SUT and gets a quote from it then checks if the returned
	 * value is positive.
	 */
	@Test
	public void testNoFreePrice() {
		CloudProvider minimalistic = new MinimalProvider();
		assertTrue("You should hand me over a positive price",
				minimalistic.getPerTickQuote(ConstantConstraints.noResources) > 0);
	}

	/**
	 * Compares the price quote from SUT with BuiltInProvider and asks it to be
	 * smaller than the one from BuiltIn.
	 */
	@Test
	public void testWithBuiltInProvider() {
		CloudProvider minimalistic = new MinimalProvider();
		CloudProvider basic = new BuiltInCloudProvider();
		double minPrice = minimalistic.getPerTickQuote(ConstantConstraints.noResources);
		double builtInPrice = basic.getPerTickQuote(ConstantConstraints.noResources);
		assertTrue("The price should be smaller", minPrice < builtInPrice);
	}

}
