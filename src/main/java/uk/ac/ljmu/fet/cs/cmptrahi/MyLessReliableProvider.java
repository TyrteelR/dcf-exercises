package uk.ac.ljmu.fet.cs.cmptrahi;

import java.util.ArrayList;
import java.util.List;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import hu.unimiskolc.iit.distsys.BuiltInCloudProvider;
import hu.unimiskolc.iit.distsys.ExercisesBase;
import hu.unimiskolc.iit.distsys.forwarders.PMForwarder;

public class MyLessReliableProvider extends BuiltInCloudProvider {

	@Override
	public double getPerTickQuote(ResourceConstraints rc) {
		// Reduce the price of builtin by some to compensate for the less
		// reliable machines we have
		return super.getPerTickQuote(rc) * 0.8;
	}

	@Override
	public void setIaaSService(IaaSService iaas) {
		// Make a copy so we can freely change the original
		ArrayList<PhysicalMachine> pmlist = new ArrayList<>(iaas.machines);
		try {
			for (PhysicalMachine pm : pmlist) {
				// We create a PM with half the original reliability
				PhysicalMachine newPM = ExercisesBase.getNewPhysicalMachine(((PMForwarder) pm).getReliMult() * 2);
				// We replace the pm with the new less reliable one
				iaas.deregisterHost(pm);
				ExercisesBase.dropPM(pm); // optional, but helps memorywise
				iaas.registerHost(newPM);
			}
		} catch (Exception e) {
			// Rethrow as runtime exception which is not noticed on the IF
			throw new RuntimeException("Deregistration issues", e);
		}
		myProvidedService=iaas;
		iaas.subscribeToCapacityChanges(this);
	}

	@Override
	public void capacityChanged(ResourceConstraints newCapacity, List<PhysicalMachine> affectedCapacity) {
		final boolean newRegistration = myProvidedService.isRegisteredHost(affectedCapacity.get(0));
		if (!newRegistration) {
			try {
				for (PhysicalMachine pm : affectedCapacity) {
					// For every lost PM we buy a new one with the exact same
					// reliability we have had before
					myProvidedService
							.registerHost(ExercisesBase.getNewPhysicalMachine(((PMForwarder) pm).getReliMult()));
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

}