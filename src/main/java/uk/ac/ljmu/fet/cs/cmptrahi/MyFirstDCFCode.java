package uk.ac.ljmu.fet.cs.cmptrahi;

import hu.mta.sztaki.lpds.cloud.simulator.Timed;

public class MyFirstDCFCode {

	public static void main(String[] args) {
		
		//how does this fit into the UML?
		PeriodicWriter periodic=new PeriodicWriter();
		periodic.setMarker(" ");
		WriteOnce once=new WriteOnce(periodic);
		once.setPayload("Hello world");
		Timed.simulateUntil(4);
		System.out.println();
	}

}
