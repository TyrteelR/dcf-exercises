package uk.ac.ljmu.fet.cs.cmpjwats;

import java.util.ArrayList;
import java.util.List;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import hu.unimiskolc.iit.distsys.interfaces.CloudProvider;

public class MyFirstProvider implements CloudProvider {

	@Override
	public double getPerTickQuote(ResourceConstraints rc) {
		return 0.0001999;
	}

	@Override
	public void setIaaSService(IaaSService iaas) {
		ArrayList<PhysicalMachine> halfOfTheMachines = new ArrayList<PhysicalMachine>(iaas.machines.subList(0, 15));
		try {
			for (PhysicalMachine pm : halfOfTheMachines) {
				iaas.deregisterHost(pm);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Unexpected things happened terminating...");
			System.exit(1);
		}
	}

	@Override
	public void capacityChanged() {
		// TODO Auto-generated method stub
		
	}

}
