package uk.ac.ljmu.fet.cs.cmpjwats.minimalvariations;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import hu.unimiskolc.iit.distsys.BuiltInCloudProvider;

public class MinimalWithSimpleInheritance extends BuiltInCloudProvider {
	/**
	 * Half the price of builtinprovider for all the resources.
	 * 
	 * @param rc
	 *            this is just ignored
	 */
	@Override
	public double getPerTickQuote(ResourceConstraints rc) {
		return 0.0001;
	}
}
