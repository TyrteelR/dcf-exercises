package uk.ac.ljmu.fet.cs.cmpjwats;


import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ConstantConstraints;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import hu.unimiskolc.iit.distsys.BuiltInCloudProvider;
import hu.unimiskolc.iit.distsys.ExercisesBase;

public class MyRCAwareProvider extends BuiltInCloudProvider {
	public static final ConstantConstraints maxPMSize = new ConstantConstraints(ExercisesBase.maxCoreCount,
			ExercisesBase.maxProcessingCap, ExercisesBase.maxMem);

	@Override
	public double getPerTickQuote(ResourceConstraints rc) {
		return (rc.getTotalProcessingPower() * rc.getRequiredMemory())
				/ (maxPMSize.getTotalProcessingPower() * maxPMSize.getRequiredMemory());
	}
}