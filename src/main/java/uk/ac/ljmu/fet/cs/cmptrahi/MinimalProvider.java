package uk.ac.ljmu.fet.cs.cmptrahi;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import hu.unimiskolc.iit.distsys.BuiltInCloudProvider;
import hu.unimiskolc.iit.distsys.interfaces.CloudProvider;

public class MinimalProvider implements CloudProvider {
private static BuiltInCloudProvider builtinInstance=null;
	/**
	 * @author cmptrahi,cmpjwats.cmpdrygo and cmpanakh
	 * below is the price for all the built in provider
	 */

@Override
	public double getPerTickQuote(ResourceConstraints rc) {
		
		
		return builtinInstance.getPerTickQuote(rc)/2;
	
 }


// no need to worry as this is not part of the coursework
	@Override
	public void setIaaSService(IaaSService iaas) {
		// TODO Auto-generated method stub

	}

	@Override
	public void capacityChanged() {
		// TODO Auto-generated method stub
	}
}
