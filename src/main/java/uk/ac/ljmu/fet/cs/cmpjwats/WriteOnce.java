package uk.ac.ljmu.fet.cs.cmpjwats;

import hu.mta.sztaki.lpds.cloud.simulator.DeferredEvent;

public class WriteOnce extends DeferredEvent {

	private String payload=null;
	private PeriodicWriter periodic;
	
	public WriteOnce(PeriodicWriter p) {
		super(2);
		periodic=p;
	}
	
	public void setPayload(final String pl) {
		payload=pl;
	}
	
	@Override
	protected void eventAction() {
		System.out.print(payload + " from DISSECT-CF");
		periodic.setMarker("!");
	}

}
