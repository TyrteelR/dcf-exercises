package uk.ac.ljmu.fet.cs.cmptrahi;
import java.util.List;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.VMManager.CapacityChangeEvent;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import hu.unimiskolc.iit.distsys.forwarders.IaaSForwarder;
import hu.unimiskolc.iit.distsys.forwarders.PMForwarder;
import hu.unimiskolc.iit.distsys.interfaces.CloudProvider;
import uk.ac.ljmu.fet.cs.cmpjwats.PMCreator;

public class MyReactiveProvider implements CloudProvider {
	private IaaSService iaas;

	@Override
	public double getPerTickQuote(ResourceConstraints rc) {
		// Solution for the optional task 2 step 7
		int count = 0;
		for (PhysicalMachine pm : iaas.machines) {
			count += pm.publicVms.size() > 0 ? 1 : 0;
		}
		return (0.0002 * count) / iaas.machines.size();
	}

	@Override
	public void setIaaSService(IaaSService iaas) {
		this.iaas = iaas;
		((IaaSForwarder) iaas).setQuoteProvider(this);
		iaas.subscribeToCapacityChanges(new CapacityChangeEvent<PhysicalMachine>() {
			@Override
			public void capacityChanged(ResourceConstraints newCapacity, List<PhysicalMachine> affectedCapacity) {
				final boolean newRegistration = iaas.isRegisteredHost(affectedCapacity.get(0));
				if (!newRegistration) {
					try {
						for (PhysicalMachine pm : affectedCapacity) {
							// For every lost PM we buy a new one.
							iaas.registerHost(PMCreator.createLowerPricedMachine((PMForwarder) pm));
						}
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			}
		});
	}

	@Override
	public void capacityChanged() {
		// TODO Auto-generated method stub
		
	}

}
