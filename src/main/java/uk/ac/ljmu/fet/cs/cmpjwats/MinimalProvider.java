package uk.ac.ljmu.fet.cs.cmpjwats;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import hu.unimiskolc.iit.distsys.CostAnalyserandPricer;
import hu.unimiskolc.iit.distsys.interfaces.CloudProvider;

public class MinimalProvider implements CloudProvider {

	/**
	 * Half the price of builtinprovider for all the resources.
	 * 
	 * @param rc
	 *            this is just ignored
	 */
	@Override
	public double getPerTickQuote(ResourceConstraints rc) {
		return 0.0001;
	}

	/**
	 * We will just ignore the iaas service as we don't do any reactive actions
	 * 
	 */
	@Override
	public void setIaaSService(IaaSService iaas) {
	}

	@Override
	public void capacityChanged() {
		// TODO Auto-generated method stub
		
	}


}
