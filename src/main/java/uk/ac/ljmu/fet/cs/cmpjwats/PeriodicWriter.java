package uk.ac.ljmu.fet.cs.cmpjwats;

import hu.mta.sztaki.lpds.cloud.simulator.Timed;

public class PeriodicWriter extends Timed {
	
	private String marker=null;
	
	public PeriodicWriter() {
		subscribe(1);
	}
	
	public void setMarker(final String m) {
		marker=m;
	}

	@Override
	public void tick(long fires) {
		System.out.print(marker);
	}

}
