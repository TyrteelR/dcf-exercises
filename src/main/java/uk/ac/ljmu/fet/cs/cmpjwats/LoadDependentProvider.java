package uk.ac.ljmu.fet.cs.cmpjwats;

import hu.mta.sztaki.lpds.cloud.simulator.DeferredEvent;
import hu.mta.sztaki.lpds.cloud.simulator.energy.MonitorConsumption;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.PhysicalMachine;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import hu.unimiskolc.iit.distsys.CostAnalyserandPricer;
import hu.unimiskolc.iit.distsys.forwarders.IaaSForwarder;
import hu.unimiskolc.iit.distsys.interfaces.CloudProvider;

public class LoadDependentProvider implements CloudProvider {
	MonitorConsumption[] monitors;

	// The actual "pricing" solution
	@Override
	public double getPerTickQuote(ResourceConstraints rc) {
		if (monitors == null || !monitors[0].isSubscribed()) {
			return 0.00002;
		}
		double currentTotalConsumption = 0;
		for (MonitorConsumption mon : monitors) {
			currentTotalConsumption += mon.getSubHourProcessing();
		}
		// This could be way more sophisticated e.g., 0.02*subhour/total or
		// similar
		return currentTotalConsumption;
	}

	// Prepares the pricing solution, and ensures helper monitors are
	// dropped once they are not needed
	@Override
	public void setIaaSService(IaaSService iaas) {
		((IaaSForwarder) iaas).setQuoteProvider(this);
		// ensuring that our monitors are in sync with the test's monitors
		new DeferredEvent(2 * 24 * 60 * 60 * 1000 + 1) {
			// the magical numbers are coming from the test case!
			@Override
			protected void eventAction() {
				monitors = new MonitorConsumption[iaas.machines.size()];
				int i = 0;
				for (PhysicalMachine pm : iaas.machines) {
					monitors[i++] = new MonitorConsumption(pm, 1000);
				}
				// the magical numbers are coming from the test case!
				new DeferredEvent(102 * 10 * 60 * 1000l) {
					@Override
					protected void eventAction() {
						// Terminate the subscription of the stopcondition
						// Then terminate all monitors
						for (MonitorConsumption mon : monitors) {
							mon.cancelMonitoring();
						}
					}
				};
			}
		};
	}

	@Override
	public void capacityChanged() {
		// TODO Auto-generated method stub
		
	}

}
