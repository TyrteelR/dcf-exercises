package uk.ac.ljmu.fet.cs.cmpjwats.minimalvariations;

import hu.mta.sztaki.lpds.cloud.simulator.iaas.IaaSService;
import hu.mta.sztaki.lpds.cloud.simulator.iaas.constraints.ResourceConstraints;
import hu.unimiskolc.iit.distsys.BuiltInCloudProvider;
import hu.unimiskolc.iit.distsys.CostAnalyserandPricer;
import hu.unimiskolc.iit.distsys.interfaces.CloudProvider;

public class MinimalWithExternalUsage implements CloudProvider {
	private static final BuiltInCloudProvider builtInInstance = new BuiltInCloudProvider();

	/**
	 * Half the price of builtinprovider for all the resources.
	 * 
	 * @param rc
	 *            this is just ignored
	 */
	@Override
	public double getPerTickQuote(ResourceConstraints rc) {
		return builtInInstance.getPerTickQuote(rc) / 2;
	}

	/**
	 * We will just ignore the iaas service as we don't do any reactive actions
	 * 
	 */
	@Override
	public void setIaaSService(IaaSService iaas) {
	}

	@Override
	public void capacityChanged() {
		// TODO Auto-generated method stub
		
	}


}
